
---
title: ggan9el package
author:
- name: Angel Martinez-Perez
- affiliation: Unit of Genomic of Complex Diseases
date: "Started: 2022-04-04"
---

Angel Martinez-Perez [![](https://info.orcid.org/wp-content/uploads/2019/11/orcid_24x24.png)](https://orcid.org/0000-0002-5934-1454)




[![pipeline status](https://gitlab.com/an9el/an9elversions/badges/main/pipeline.svg)](https://gitlab.com/an9el/an9elversions/-/commits/main)
[![coverage report](https://gitlab.com/an9el/an9elversions/badges/main/coverage.svg)](https://gitlab.com/an9el/an9elversions/-/commits/main)

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![Last-changedate](https://img.shields.io/badge/last%20change-2024--11--13-yellowgreen.svg)](/commits/master)
<!-- badges: end -->
[[_TOC_]]


# Installation

Some packages need to be installed first:



```r
BiocManager::install("ggplot2")
remotes::install_github("wilkelab/ggtext")
remotes::install_gitlab("an9el/an9elutils")
remotes::install_gitlab("an9el/ggan9el")
```


# manhattan plots

Use of [ggfastman](https://rdrr.io/github/roman-tremmel/FASTGWASMAN/f/README.md) package

> devtools::install_github("roman-tremmel/ggfastman")

Data must have "chr", "pos" and "pvalue" columns, chr in format "chr1", "chr2", ...
Also column "rsid" if we want to highlight some variants. No `chrX` is plotted 




```r
library(data.table)
library(ggfastman)
library(tidyverse)
library(magrittr)
library(ggrepel)
dt <- data.table::fread("your_gwas_summary_data.csv.gz")
dt_nX <- dt %>% dplyr::filter(chr != "chr23")
mn <- ggfastman::fast_manhattan(dt_nX, build = 'hg38',
                                speed = "slow",
                                highlight = dt[dt$pvalue < 5e-8, ]$rsid) +
    geom_hline(yintercept = -log10(5e-08),
               linetype = 2, color = "deeppink") +
    ggrepel::geom_label_repel(data = . %>% dplyr::filter(pvalue <= 5e-8) %>%
                                  group_by(chr) %>% top_n(1, -pvalue),
                              aes(label = rsid),
                              color = 1,
                              min.segment.length = 0)

png("manhattan.png", width = 1920, height = 960)
mn
dev.off()
```


## qqplot



```r
qq <- ggfastman::fast_qq(pvalue = dt_nX$pvalue, speed = "slow")
png("DB/plots/EA2_Discovery_original_qqplot.png", width = 960, height = 960)
qq
dev.off()
```


# locuszoom plots




```r
dtt <- dt_nX %>% dplyr::filter(chr == "chr1")
lp <- ggfastman::fast_locusplot(dtt, token = "your_token_here",
                     show_MAF = TRUE,
                     show_regulom = TRUE,
                     build = "hg38", # "hg19"
                     color = "viridis")

png("locusplot_chr1.png", width = 1200, height = 1200)
lp
dev.off()
```


# SESSION INFO


<details closed>
<summary> <span title='Clik to Expand'> Current session info </span> </summary>

```r

─ Session info ───────────────────────────────────────────────────────────────
 setting  value
 version  R version 4.4.2 (2024-10-31)
 os       Ubuntu 24.04.1 LTS
 system   x86_64, linux-gnu
 ui       X11
 language (EN)
 collate  es_ES.UTF-8
 ctype    es_ES.UTF-8
 tz       Europe/Madrid
 date     2024-11-13
 pandoc   3.1.3 @ /usr/bin/ (via rmarkdown)

─ Packages ───────────────────────────────────────────────────────────────────
 ! package      * version    date (UTC) lib source
   an9elproject   0.9.0      2024-09-02 [2] gitlab (an9el/an9elproject@bc85cdd)
   an9elutils   * 0.7.0      2024-10-23 [2] gitlab (an9el/an9elutils@cfbb18e)
   askpass        1.2.1      2024-10-04 [2] CRAN (R 4.4.1)
   backports      1.5.0      2024-05-23 [2] CRAN (R 4.4.0)
   cachem         1.1.0      2024-05-16 [2] CRAN (R 4.4.0)
   caret          6.0-94     2023-03-21 [2] CRAN (R 4.3.1)
   checkmate      2.3.2      2024-07-29 [2] CRAN (R 4.4.1)
   class          7.3-22     2023-05-03 [4] CRAN (R 4.4.0)
   cli            3.6.3      2024-06-21 [2] CRAN (R 4.4.1)
   clipr          0.8.0      2022-02-22 [2] CRAN (R 4.3.1)
   codetools      0.2-20     2024-03-31 [2] CRAN (R 4.3.1)
   colorspace     2.1-1      2024-07-26 [2] CRAN (R 4.4.1)
   commonmark     1.9.2      2024-10-04 [2] CRAN (R 4.4.1)
   data.table     1.16.2     2024-10-10 [2] CRAN (R 4.4.1)
   datawizard     0.13.0     2024-10-05 [2] CRAN (R 4.4.1)
   desc           1.4.3      2023-12-10 [2] CRAN (R 4.3.1)
   details      * 0.3.0      2022-03-27 [2] CRAN (R 4.3.1)
   devtools       2.4.5      2022-10-11 [2] CRAN (R 4.3.1)
   digest         0.6.37     2024-08-19 [2] CRAN (R 4.4.1)
   dplyr          1.1.4      2023-11-17 [2] CRAN (R 4.3.1)
   ellipsis       0.3.2      2021-04-29 [2] CRAN (R 4.3.1)
   evaluate       1.0.1      2024-10-10 [2] CRAN (R 4.4.1)
   fansi          1.0.6      2023-12-08 [2] CRAN (R 4.3.1)
   fastmap        1.2.0      2024-05-15 [2] CRAN (R 4.4.0)
   foreach        1.5.2      2022-02-02 [2] CRAN (R 4.3.1)
   fs             1.6.4      2024-04-25 [2] CRAN (R 4.3.1)
   future         1.34.0     2024-07-29 [2] CRAN (R 4.4.1)
   future.apply   1.11.3     2024-10-27 [2] CRAN (R 4.4.1)
   generics       0.1.3      2022-07-05 [2] CRAN (R 4.3.1)
 P ggan9el      * 0.1.0      2024-11-13 [?] gitlab (an9el/ggan9el@4b3aa98)
   ggplot2      * 3.5.1      2024-04-23 [2] CRAN (R 4.3.1)
   ggtext       * 0.1.2      2022-09-16 [2] CRAN (R 4.3.1)
   globals        0.16.3     2024-03-08 [2] CRAN (R 4.3.1)
   glue         * 1.8.0      2024-09-30 [2] CRAN (R 4.4.1)
   gower          1.0.1      2022-12-22 [2] CRAN (R 4.3.1)
   gridExtra      2.3        2017-09-09 [2] CRAN (R 4.3.1)
   gridtext       0.1.5      2022-09-16 [2] CRAN (R 4.3.1)
   gtable         0.3.6      2024-10-25 [2] CRAN (R 4.4.1)
   hardhat        1.4.0      2024-06-02 [2] CRAN (R 4.4.0)
   here         * 1.0.1      2020-12-13 [2] CRAN (R 4.4.1)
   htmltools      0.5.8.1    2024-04-04 [2] CRAN (R 4.3.1)
   htmlwidgets    1.6.4      2023-12-06 [2] CRAN (R 4.3.1)
   httpuv         1.6.15     2024-03-26 [2] CRAN (R 4.3.1)
   httr           1.4.7      2023-08-15 [2] CRAN (R 4.3.1)
   insight        0.20.5     2024-10-02 [2] CRAN (R 4.4.1)
   ipred          0.9-15     2024-07-18 [2] CRAN (R 4.4.1)
   iterators      1.0.14     2022-02-05 [2] CRAN (R 4.3.1)
   jsonlite       1.8.9      2024-09-20 [2] CRAN (R 4.4.1)
   knitr        * 1.48       2024-07-07 [2] CRAN (R 4.4.1)
   later          1.3.2      2023-12-06 [2] CRAN (R 4.3.1)
   lattice        0.22-6     2024-03-20 [2] CRAN (R 4.3.1)
   lava           1.8.0      2024-03-05 [2] CRAN (R 4.3.1)
   lifecycle      1.0.4      2023-11-07 [2] CRAN (R 4.3.1)
   listenv        0.9.1      2024-01-29 [2] CRAN (R 4.3.1)
   lubridate      1.9.3      2023-09-27 [2] CRAN (R 4.3.1)
   magrittr       2.0.3      2022-03-30 [2] CRAN (R 4.3.1)
   MASS           7.3-61     2024-06-13 [2] CRAN (R 4.4.0)
   Matrix         1.7-1      2024-10-18 [2] CRAN (R 4.4.1)
   memoise        2.0.1      2021-11-26 [2] CRAN (R 4.3.1)
   mime           0.12       2021-09-28 [2] CRAN (R 4.3.1)
   miniUI         0.1.1.1    2018-05-18 [2] CRAN (R 4.3.1)
   ModelMetrics   1.2.2.2    2020-03-17 [2] CRAN (R 4.3.1)
   munsell        0.5.1      2024-04-01 [2] CRAN (R 4.3.1)
   naniar         1.1.0      2024-03-05 [2] CRAN (R 4.3.1)
   nlme           3.1-166    2024-08-14 [4] CRAN (R 4.4.1)
   nnet           7.3-19     2023-05-03 [4] CRAN (R 4.3.3)
   openssl        2.2.2      2024-09-20 [2] CRAN (R 4.4.1)
   pacman       * 0.5.1      2019-03-11 [2] CRAN (R 4.3.1)
   parallelly     1.38.0     2024-07-27 [2] CRAN (R 4.4.1)
   pillar         1.9.0      2023-03-22 [2] CRAN (R 4.3.1)
   pkgbuild       1.4.4      2024-03-17 [2] CRAN (R 4.3.1)
   pkgconfig      2.0.3      2019-09-22 [2] CRAN (R 4.3.1)
   pkgload        1.4.0      2024-06-28 [2] CRAN (R 4.4.1)
   plyr           1.8.9      2023-10-02 [2] CRAN (R 4.3.1)
   png            0.1-8      2022-11-29 [2] CRAN (R 4.3.1)
   pROC           1.18.5     2023-11-01 [2] CRAN (R 4.3.1)
   prodlim        2024.06.25 2024-06-24 [2] CRAN (R 4.4.1)
   profvis        0.4.0      2024-09-20 [2] CRAN (R 4.4.1)
   promises       1.3.0      2024-04-05 [2] CRAN (R 4.3.1)
   purrr          1.0.2      2023-08-10 [2] CRAN (R 4.3.1)
   R6             2.5.1      2021-08-19 [2] CRAN (R 4.3.1)
   Rcpp           1.0.13-1   2024-11-02 [2] CRAN (R 4.4.2)
   recipes        1.1.0      2024-07-04 [2] CRAN (R 4.4.1)
   remotes        2.5.0      2024-03-17 [2] CRAN (R 4.3.1)
   reshape2       1.4.4      2020-04-09 [2] CRAN (R 4.3.1)
   rlang          1.1.4      2024-06-04 [2] CRAN (R 4.4.0)
   rmarkdown    * 2.28       2024-08-17 [2] CRAN (R 4.4.1)
   roxygen2       7.3.2      2024-06-28 [2] CRAN (R 4.4.1)
   rpart          4.1.23     2023-12-05 [4] CRAN (R 4.4.0)
   rprojroot      2.0.4      2023-11-05 [2] CRAN (R 4.3.1)
   rstudioapi     0.17.1     2024-10-22 [2] CRAN (R 4.4.1)
   scales         1.3.0      2023-11-28 [2] CRAN (R 4.3.1)
   sessioninfo    1.2.2      2021-12-06 [2] CRAN (R 4.3.1)
   shiny          1.9.1      2024-08-01 [2] CRAN (R 4.4.1)
   stringi        1.8.4      2024-05-06 [2] CRAN (R 4.3.3)
   stringr        1.5.1      2023-11-14 [2] CRAN (R 4.3.1)
   survival       3.7-0      2024-06-05 [4] CRAN (R 4.4.0)
   tibble         3.2.1      2023-03-20 [2] CRAN (R 4.3.1)
   tidyselect     1.2.1      2024-03-11 [2] CRAN (R 4.3.1)
   timechange     0.3.0      2024-01-18 [2] CRAN (R 4.3.1)
   timeDate       4041.110   2024-09-22 [2] CRAN (R 4.4.1)
   units          0.8-5      2023-11-28 [2] CRAN (R 4.3.1)
   urlchecker     1.0.1      2021-11-30 [2] CRAN (R 4.3.1)
   usethis        3.0.0      2024-07-29 [2] CRAN (R 4.4.1)
   utf8           1.2.4      2023-10-22 [2] CRAN (R 4.3.1)
   vctrs          0.6.5      2023-12-01 [2] CRAN (R 4.3.1)
   vetr           0.2.18     2024-06-21 [2] CRAN (R 4.4.1)
   viridis        0.6.5      2024-01-29 [2] CRAN (R 4.3.1)
   viridisLite    0.4.2      2023-05-02 [2] CRAN (R 4.3.1)
   visdat         0.6.0      2023-02-02 [2] CRAN (R 4.3.1)
   visNetwork     2.1.2      2022-09-29 [2] CRAN (R 4.3.1)
   withr          3.0.2      2024-10-28 [2] CRAN (R 4.4.2)
   xfun           0.49       2024-10-31 [2] CRAN (R 4.4.2)
   xml2           1.3.6      2023-12-04 [2] CRAN (R 4.3.1)
   xtable         1.8-4      2019-04-21 [2] CRAN (R 4.3.1)
   yaml           2.3.10     2024-07-26 [2] CRAN (R 4.4.1)

 [1] /home/amartinezp/R/x86_64-pc-linux-gnu-library/4.4
 [2] /usr/local/lib/R/site-library
 [3] /usr/lib/R/site-library
 [4] /usr/lib/R/library

 P ── Loaded and on-disk path mismatch.

──────────────────────────────────────────────────────────────────────────────

```

</details>
<br>

