# ggan9el 0.1.0

* added simpleQQplot function
* Added plot_pedigree function
* Added get_population_colors to plot the 1000G project


# ggan9el 0.0.0.9000

* Added geom_points plots
* Added a `NEWS.md` file to track changes to the package.
